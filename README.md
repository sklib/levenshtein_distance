# LevenshteinDistance

`LevenshteinDistance` is a module to calculate Levenshtein Distance between two strings. And two arrays which contain Integer instance also be handled.

This module is a native extention written in C++ and based on the source code published on [Wikibooks](https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance).
So this module also be published under the license of [Creative Commons Attribution-ShareAlike License](https://creativecommons.org/licenses/by-sa/3.0/).

## Installation

To install this module, execute the following instractions.

```shell
git clone https://bitbucket.org/sklib/levenshtein_distance
cd levenshtein_distance
rake install
```

These instructions shall create `pkg/levenshtein_distance-X.Y.Z.gem`. You can install it to your Ruby environment.

```shell
gen install pkg/levenshtein_distance-X.Y.Z.gem
```

If you won't to install, you can manually place `lib` directory into arbitrary path.

```shell
cp -r lib path/to/you/want/to/place
```

And set the path in RUBYLIB or require the module by full-path.

```ruby
require 'path/to/you/want/to/place/levenshtein_distance'
```

## Usage

At first, you must require the module in your script so that the module LevenshteinDistance be ready.

```ruby
require 'levenshtein_distance'
```

The module LevenshteinDistance includes only 3 singleton methods as follows.

```ruby
LevenshtainDistance.distance_string(source, target, insert_cost, delete_cost, replace_cost)
LevenshtainDistance.distance_ascii_string(source, target, insert_cost, delete_cost, replace_cost)
LevenshtainDistance.distance_int_array(source, target, insert_cost, delete_cost, replace_cost)
```

distance_string 
: Calculate the distance between two strings given as `source` and `target` encoded by UTF8 or ASCII.

distance_ascii_string
: Calculate the distance between two strings given as `source` and `target` encoded by ASCII. It is faster than `distance_string`.

distance_int_array
: Calculate the distance between two arrays contain Integer instances.

## Contributing

To be written.

## License

The gem is available as open source under the terms of the [Creative Commons Attribution-ShareAlike License](https://creativecommons.org/licenses/by-sa/3.0/).
