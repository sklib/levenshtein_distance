#include <stdio.h>
#include <stdlib.h>

#include <algorithm>
#include <vector>
#include <string>

template<typename T>
typename T::size_type GeneralizedLevensteinDistance(const T &source,
                                                    const T &target,
                                                    typename T::size_type insert_cost = 1,
                                                    typename T::size_type delete_cost = 1,
                                                    typename T::size_type replace_cost = 1) {
    if (source.size() > target.size()) {
        return GeneralizedLevensteinDistance(target, source, delete_cost, insert_cost, replace_cost);
    }

    using TSizeType = typename T::size_type;
    const TSizeType min_size = source.size(), max_size = target.size();
    std::vector<TSizeType> lev_dist(min_size + 1);

    lev_dist[0] = 0;
    for (TSizeType i = 1; i <= min_size; ++i) {
        lev_dist[i] = lev_dist[i - 1] + delete_cost;
    }

    for (TSizeType j = 1; j <= max_size; ++j) {
        TSizeType previous_diagonal = lev_dist[0], previous_diagonal_save;
        lev_dist[0] += insert_cost;

        for (TSizeType i = 1; i <= min_size; ++i) {
            previous_diagonal_save = lev_dist[i];
            if (source[i - 1] == target[j - 1]) {
                lev_dist[i] = previous_diagonal;
            } else {
                lev_dist[i] = std::min(std::min(lev_dist[i - 1] + delete_cost, lev_dist[i] + insert_cost), previous_diagonal + replace_cost);
            }
            previous_diagonal = previous_diagonal_save;
        }
    }

    return lev_dist[min_size];
}

#include "ruby.h"

extern "C" {
    VALUE rb_mLevenshteinDistance = Qnil;

    static void ConvertIntArrayToVector(VALUE array, std::vector<int64_t> &vector)
    {
        VALUE *ptr = RARRAY_PTR(array);
        int len = RARRAY_LEN(array);
        for (int i = 0; i < len; ++i) {
            vector.push_back((int64_t)NUM2LL(*(ptr++)));
        }
    }

    static VALUE method_distance_int_array(VALUE self, VALUE array_source, VALUE array_target, VALUE insert_cost, VALUE delete_cost, VALUE replace_cost)
    {
        std::vector<int64_t> source;
        ConvertIntArrayToVector(array_source, source);

        std::vector<int64_t> target;
        ConvertIntArrayToVector(array_target, target);

        std::vector<int64_t>::size_type ret
            = GeneralizedLevensteinDistance(source,
                                            target,
                                            (std::vector<int64_t>::size_type)NUM2INT(insert_cost),
                                            (std::vector<int64_t>::size_type)NUM2INT(delete_cost),
                                            (std::vector<int64_t>::size_type)NUM2INT(replace_cost));
        return INT2NUM((int)ret);
    }

    static VALUE method_distance_ascii_string(VALUE self, VALUE string_source, VALUE string_target, VALUE insert_cost, VALUE delete_cost, VALUE replace_cost)
    {
        std::string source(StringValueCStr(string_source));
        std::string target(StringValueCStr(string_target));
        std::string::size_type ret
            = GeneralizedLevensteinDistance(source,
                                            target,
                                            (std::string::size_type)NUM2INT(insert_cost),
                                            (std::string::size_type)NUM2INT(delete_cost),
                                            (std::string::size_type)NUM2INT(replace_cost));
        return INT2NUM((int)ret);
    }

    void Init_levenshtein_distance(void)
    {
        rb_mLevenshteinDistance = rb_define_module("LevenshteinDistance");
        rb_define_singleton_method(rb_mLevenshteinDistance,
                                   "distance_int_array",
                                   (VALUE(*)(ANYARGS))method_distance_int_array,
                                   5);
        rb_define_singleton_method(rb_mLevenshteinDistance,
                                   "distance_ascii_string",
                                   (VALUE(*)(ANYARGS))method_distance_ascii_string,
                                   5);
    }
} /* extern "C" */
