require "levenshtein_distance/version"
require "levenshtein_distance/levenshtein_distance"

module LevenshteinDistance
  class Error < StandardError; end

  def self.distance_string(source, target, insert_cost, delete_cost, replace_cost)
    return distance_int_array(source.unpack("U*"),
                              target.unpack("U*"),
                              insert_cost,
                              delete_cost,
                              replace_cost)
  end
end
